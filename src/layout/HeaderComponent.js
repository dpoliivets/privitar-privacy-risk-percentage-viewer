// Import dependencies
import React from 'react'

// Import components
import { Spin } from 'antd'
import { Header, TitleText } from '../styled-components/UILibrary'


export default class HeaderComponent extends React.Component {
    render() {
        return (
            <Header>
                <TitleText color='#ffffff'>
                    Privitar Privacy Protector
                </TitleText>
                {
                    this.props.isLoading ?
                        <Spin size='large' />
                        :
                        <TitleText color='#ffffff'>
                            {this.props.peopleProtected} people protected
                        </TitleText>
                }
            </Header>
        );
    }
}
