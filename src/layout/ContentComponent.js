// Import dependencies
import React from 'react'

// Import styles
import './ContentComponent.css'

// Import components
import UserCard from '../components/UserCard'
import { Content } from '../styled-components/UILibrary'
import { Collapse, Spin } from 'antd'
const { Panel } = Collapse

const bottomMargin = {
    marginBottom: 20
}


export default class ContentComponent extends React.Component {
    render() {
        return (
            <Content className='cc__container'>
                {/* Check if data is loaded */}
                {
                    this.props.isLoading ?
                        <div className='cc__loader-container'>
                            <Spin size='large' />
                        </div>
                        :
                        <Collapse
                            accordion
                            bordered={false}
                            className='cc__accordion-container'
                        >
                            {
                                // Render fetched data
                                this.props.data.map((data, index) =>
                                    <Panel
                                        header={data.first_name + ' ' + data.last_name}
                                        key={index}
                                        className='cc__accordion-panel'
                                        style={bottomMargin}
                                    >
                                        <UserCard
                                            age={data.age}
                                            nationality={data.nationality}
                                            risk={data.risk_percentage}
                                            name={data.first_name}
                                        />
                                    </Panel>
                                )
                            }
                        </Collapse>
                }
            </Content>
        );
    }
}
