// Import dependencies
import React from 'react'

// Import components
import { Avatar } from 'antd'
import { ParagraphText } from '../styled-components/UILibrary'

// Import styles
import './UserCard.css'


/*
    Component that displays a user card
*/
const UserCard = (props) => {
    return (
        <div>
            {/* User info container */}
            <div className='user-card__info-container'>
                <Avatar size={40} className='user-card__avatar'>
                    {props.name}
                </Avatar>

                <div className='user-card__text-info'>
                    <ParagraphText color='#252525' bold>
                        Age
                    </ParagraphText>
                    <ParagraphText color='#595959' margin='10px 0 0 0'>
                        {props.age}
                    </ParagraphText>
                </div>
                <div className='user-card__text-info'>
                    <ParagraphText color='#252525' bold>
                        Nationality
                    </ParagraphText>
                    <ParagraphText color='#595959' margin='10px 0 0 0'>
                        {props.nationality}
                    </ParagraphText>
                </div>
            </div>

            {/* Privacy Risk Indicator */}
            <div className='user-card__privacy-risk-container'>
                <div className='user-card__privacy-risk-title'>
                    <ParagraphText color='#252525' bold>
                        Privacy Risk
                    </ParagraphText>
                    <ParagraphText color='#252525'>
                        {props.risk + '%'}
                    </ParagraphText>
                </div>
                <div className='user-card__risk-inicator-container'>
                    <div
                        className='user-card__risk-inicator-value'
                        style={{ 
                            width: props.risk + '%',
                            backgroundColor: props.risk < 20 ? '#A1FFAB' : props.risk < 30 ? '#F9F48F' : '#FF9A9A'
                        }}
                    />
                </div>
            </div>
        </div>
    );
};

export default UserCard;
