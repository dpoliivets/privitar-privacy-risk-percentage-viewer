# Privitar Privacy Risk Percentage Viewer Exercise
By Dmytro Poliyivets.


### Installation

You’ll need to have Node 8.16.0 or Node 10.16.0 or later version on your local machine to run this code.

Install the dependencies and start the server.

```sh
$ cd privitar-privacy-risk-percentage-viewer
$ npm install
$ npm start
```