// Import dependencies
import React from 'react'

// Import components
import HeaderComponent from './HeaderComponent'
import ContentComponent from './ContentComponent'
import FooterComponent from './FooterComponent'
import { PageContainer } from '../styled-components/UILibrary'
import { notification, Button } from 'antd'

const axios = require('axios');


export default class PrivacyProtectorLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            peopleProtected: 0
        }
    }

    // Fetches data from external API
    async fetchData() {
        try {
            const result = await axios.get(
                'https://m37ov7xhd3.execute-api.eu-west-1.amazonaws.com/default/ui_example_data_endpoint',
                { headers: { 'x-api-key': 'MbXge4M88v21C0ZV4utTIa1RH43UNlVkam1E7Gwp' } }
            );
            this.setState({
                data: result.data.body.people,
                isLoading: false,
                peopleProtected: result.data.body.people.length
            });
        } catch (error) {
            // Show error notification
            this.showErrorNotification();
        }
    }

    // Renders an error notification in case fetch was unsuccessful
    showErrorNotification = () => {
        const key = `open${Date.now()}`;
        const btn = (
            <Button type="primary" size="medium" onClick={() => this.fetchData()}>
                Try Again
          </Button>
        );
        notification.open({
            message: 'Server is unresponsive',
            description: 'Data could not be loaded from the server, please try again.',
            btn,
            key,
        });
    }

    componentDidMount() {
        this.fetchData();
    }

    
    render() {
        return (
            <PageContainer>
                <HeaderComponent
                    isLoading={this.state.isLoading}
                    peopleProtected={this.state.peopleProtected}
                />

                <ContentComponent
                    isLoading={this.state.isLoading}
                    data={this.state.data}
                />

                <FooterComponent />
            </PageContainer>
        );
    }
}
