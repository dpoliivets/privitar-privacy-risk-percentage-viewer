// Import dependencies
import React from 'react'

// Import components
import { Footer, ParagraphText } from '../styled-components/UILibrary'


export default class FooterComponent extends React.Component {
    render() {
        return (
            <Footer>
                <ParagraphText color='#ffffff'>
                    © Privitar 2019
                </ParagraphText>
            </Footer>
        );
    }
}
