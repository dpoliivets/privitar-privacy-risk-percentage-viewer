// Import dependencies
import styled from 'styled-components'

/*
    Page layout components
*/
export const PageContainer = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    flex-direction: column;
`;
export const Header = styled.header`
    width: 100%;
    height: 200px;
    position: fixed;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-left: 50px;
    padding-right: 50px;
    background-color: #3056EB;

    @media (max-width: 630px) {
        flex-direction: column
        justify-content: space-evenly;
        align-items: flex-start;
    }
    @media (max-width: 576px) {
        height: 130px;
    }
`;
export const Content = styled.main`
    width: 800px;
    height: calc(100% - 300px);
    position: fixed;
    top: 200px;
    align-self: center;
    background-color: #F3F5F9;

    @media (max-width: 800px) {
        width: 100%;
    }
    @media (max-width: 576px) {
        top: 130px;
        height: calc(100% - 230px);
    }
`;
export const Footer = styled.footer`
    width: 800px;
    height: 100px;
    position: fixed;
    bottom: 0;
    align-self: center;
    padding-left: 50px;
    padding-right: 50px;
    display: flex;
    align-items: center;
    background-color: #3056EB;

    @media (max-width: 800px) {
        width: 100%;
    }
`;

/*
    Fluid text components
*/
export const TitleText = styled.h1`
    font-family: 'Montserrat', sans-serif;
    font-weight: 600;
    font-size: calc(18px + (26 - 18) * ((100vw - 300px) / (1600 - 300)));
    line-height: normal;
    color: ${ props => props.color};
    margin: 0 !important;
`;
export const ParagraphText = styled.p`
    font-family: 'Montserrat', sans-serif;
    font-weight: 400;
    font-size: calc(12px + (14 - 12) * ((100vw - 300px) / (1600 - 300)));
    line-height: normal;
    color: ${ props => props.color};
    margin: 0 !important;
    ${ props => props.bold ? 'font-weight: 600' : ''};
    ${ props => props.margin ? 'margin: '+props.margin+' !important' : ''};
`;
