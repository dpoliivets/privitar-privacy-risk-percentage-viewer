// Import dependencies
import React from 'react'

// Import components
// import Button from 'antd/es/button'
import PrivacyProtectorLayout from './layout/PrivacyProtectorLayout'

// Import styles
import './App.css'

function App() {
  return (
    <PrivacyProtectorLayout />
  );
}

export default App;
